﻿var renamerData = new Object();
renamerData.scriptName = "comp renamer";
var fileName = getFilename();

//gets and returns the .aep project filename and strips the '.aep' off the name.
function getFilename()
{
    projName=app.project.file.name;
    projName = app.project.file.name.replace(/\.(.{3,4}$)/, "");
    //clearOutput();
    return projName;
}

function renObjects(){
    //renaming undo group begins here/
    app.beginUndoGroup(renamerData.scriptName);
	var everyItem = app.project.items;
    var proj = app.project;
    var selComps = new Array()
	
    // Find all project items that are of type composition and add them to a selected collection
	for (var i = everyItem.length; i >= 1; i--)
    {
        eyeTem = everyItem[i];
        if(eyeTem instanceof CompItem)
                {
                  selComps[selComps.length] = eyeTem;
                }
    }
//use the selected comps and apply filename to the compositions
    for (var n = selComps.length-1; n>=0; n--){
        originalName = selComps[n].name;
        joinedName = fileName + '_' + originalName;  
        selComps[n].name = joinedName;
        }
    app.endUndoGroup();
}

renObjects();
writeLn("shi2t");
/*  //proj.item(i).name
                    var joinedName = "";*/
                    
